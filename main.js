const questionElement = document.getElementById("question");
const answerElement = document.getElementById("answer");
const checkButton = document.getElementById("check");
const resultElement = document.getElementById("result");
const scoreElement = document.getElementById("score");

const quiz = [
    {question: "1. Who is the coach of Gilas Pilipinas during the FIBA World Cup 2023?", 
    answer: "Chot Reyes"},
    {question: "2. What animal did the NBI use to test the SIM card registration?", 
    answer: "Monkey"},
    {question: "3. What is the new name of the Twitter app?", 
    answer: "X"},
    {question: "4. What is the new name of the Twitter app?", 
    answer: "X"},
    {question: "5. What is the new name of the Twitter app?", 
    answer: "X"},
];

let currentQuestion = 0;
let score = 0;

function displayQuestion() {
    questionElement.textContent = quiz[currentQuestion].question;
    scoreElement.textContent = 'Your score: ' + score;
}

function checkAnswer() {
    const userAnswer = answerElement.value.trim();
    const correctAnswer = quiz[currentQuestion].answer;

    if (userAnswer.toLowerCase() === correctAnswer.toLowerCase()) {
        resultElement.textContent = "You are correct!";
        score++;
    } else {
        resultElement.textContent = "You are wrong!";
    }

    answerElement.value = '';
    currentQuestion++;

    if (currentQuestion < quiz.length) {
        displayQuestion();
    } else {
        questionElement.textContent = '';
        answerElement.style.display = 'none';
        checkButton.style.display = 'none';
        resultElement.textContent = '';
        scoreElement.textContent = 'Total Score: ' + score;
    }
}

displayQuestion();

checkButton.addEventListener('click', checkAnswer);